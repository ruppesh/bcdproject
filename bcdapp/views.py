from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *


class AdminLogout(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class AdminLogin(FormView):
    template_name = "bcdapp/admintemplates/adminlogin.html"
    form_class = AdminLoginForm
    success_url = '/school-admin/'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_superuser:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'form': form,
                'errors': 'Incorrect username or password'})
        return super().form_valid(form)





class AdminMixin(LoginRequiredMixin, object):
    login_url = '/school-admin/login/'

    def get_context_data(self, *args, **kwargs):
        context = super(AdminMixin, self).get_context_data(*args, **kwargs)
        context['organization'] = Organization.objects.get(id=1)
        return context


class DeleteMixin(LoginRequiredMixin, UpdateView):
    login_url = '/school-admin/login/'

    def form_valid(self, form):
        form.instance.deleted_at = timezone.now()
        form.save()
        return super().form_valid(form)


class AdminHomeView(AdminMixin, TemplateView):
    template_name = 'bcdapp/admintemplates/adminhome.html'

class AdminOrganizationCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminorganizationcreate.html'
    form_class = AdminOrganizationCreateForm
    success_url = reverse_lazy('bfsapp:adminhome')


class AdminOrganizationUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminorganizationcreate.html'
    model = Organization
    form_class = AdminOrganizationCreateForm
    success_url = reverse_lazy('bfsapp:adminhome')


class AdminBlogListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminbloglist.html'
    queryset = Blog.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allblogs'


class AdminBlogCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminblogcreate.html'
    form_class = AdminBlogCreateForm
    success_url = reverse_lazy('bcdapp:adminbloglist')


class AdminBlogUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminblogcreate.html'
    model = Blog
    form_class = AdminBlogCreateForm
    success_url = reverse_lazy('bcdapp:adminbloglist')


class AdminBlogDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminblogdelete.html'
    model = Blog
    form_class = AdminBlogDeleteForm
    success_url = reverse_lazy('bcdapp:adminbloglist')


class AdminEventListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/admineventlist.html'
    queryset = Event.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allevents'


class AdminEventCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/admineventcreate.html'
    form_class = AdminEventCreateForm
    success_url = reverse_lazy('bcdapp:admineventlist')


class AdminEventUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/admineventcreate.html'
    model = Event
    form_class = AdminEventCreateForm
    success_url = reverse_lazy('bcdapp:admineventlist')


class AdminEventDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/admineventdelete.html'
    model = Event
    form_class = AdminEventDeleteForm
    success_url = reverse_lazy('bcdapp:admineventlist')


class AdminGalleryListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/admingallerylist.html'
    queryset = Gallery.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allgallery'


class AdminGalleryCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/admingallerycreate.html'
    form_class = AdminGalleryCreateForm
    success_url = reverse_lazy('bcdapp:admingallerylist')


class AdminGalleryUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/admingallerycreate.html'
    model = Gallery
    form_class = AdminGalleryCreateForm
    success_url = reverse_lazy('bcdapp:admingallerylist')


class AdminGalleryDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/admingallerydelete.html'
    model = Gallery
    form_class = AdminGalleryDeleteForm
    success_url = reverse_lazy('bcdapp:admingallerylist')


class AdminNoticeListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminnoticelist.html'
    queryset = Notice.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allnotices'


class AdminNoticeCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminnoticecreate.html'
    form_class = AdminNoticeCreateForm
    success_url = reverse_lazy('bcdapp:adminnoticelist')


class AdminNoticeUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminnoticecreate.html'
    model = Notice
    form_class = AdminNoticeCreateForm
    success_url = reverse_lazy('bcdapp:adminnoticelist')


class AdminNoticeDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminnoticedelete.html'
    model = Notice
    form_class = AdminNoticeDeleteForm
    success_url = reverse_lazy('bcdapp:adminnoticelist')


class AdminStaffListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminstafflist.html'
    queryset = Staff.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allstaffs'


class AdminStaffCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminstaffcreate.html'
    form_class = AdminStaffCreateForm
    success_url = reverse_lazy('bcdapp:adminstafflist')


class AdminStaffUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminstaffcreate.html'
    model = Staff
    form_class = AdminStaffCreateForm
    success_url = reverse_lazy('bcdapp:adminstafflist')


class AdminStaffDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminstaffdelete.html'
    model = Staff
    form_class = AdminStaffDeleteForm
    success_url = reverse_lazy('bcdapp:adminstafflist')


class AdminTestimonialListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/admintestimoniallist.html'
    queryset = Testimonial.objects.filter(
        deleted_at__isnull=True).order_by('-id')
    context_object_name = 'alltestimonials'


class AdminTestimonialCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/admintestimonialcreate.html'
    form_class = AdminTestimonialCreateForm
    success_url = reverse_lazy('bcdapp:admintestimoniallist')


class AdminTestimonialUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/admintestimonialcreate.html'
    model = Testimonial
    form_class = AdminTestimonialCreateForm
    success_url = reverse_lazy('bcdapp:admintestimoniallist')


class AdminTestimonialDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/admintestimonialdelete.html'
    model = Testimonial
    form_class = AdminTestimonialDeleteForm
    success_url = reverse_lazy('bcdapp:admintestimoniallist')




class AdminFeaturesListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminfeatureslist.html'
    queryset = Features.objects.filter(deleted_at__isnull=True).order_by('-id')
    context_object_name = 'allfeatures'


class AdminFeaturesCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminfeaturescreate.html'
    form_class = AdminFeaturesCreateForm
    success_url = reverse_lazy('bcdapp:adminfeatureslist')


class AdminFeaturesUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminfeaturesreate.html'
    model = Features
    form_class = AdminFeaturesCreateForm
    success_url = reverse_lazy('bcdapp:adminfeatureslist')


class AdminFeaturesDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminfeaturesdelete.html'
    model = Features
    form_class = AdminFeaturesDeleteForm
    success_url = reverse_lazy('bcdapp:adminfeatureslist')



class AdminRoutineListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminroutinelist.html'
    queryset = Routine.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allroutines'


class AdminRoutineCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminroutinecreate.html'
    form_class = AdminRoutineCreateForm
    success_url = reverse_lazy('bcdapp:adminroutinelist')


class AdminRoutineUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminroutinecreate.html'
    model = Routine
    form_class = AdminRoutineCreateForm
    success_url = reverse_lazy('bcdapp:adminroutinelist')


class AdminRoutineDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminroutinedelete.html'
    model = Routine
    form_class = AdminRoutineDeleteForm
    success_url = reverse_lazy('bcdapp:adminroutinelist')



class AdminResultListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminresultlist.html'
    queryset = Result.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allresults'


class AdminResultCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminresultcreate.html'
    form_class = AdminResultCreateForm
    success_url = reverse_lazy('bcdapp:adminresultlist')


class AdminResultUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminresultcreate.html'
    model = Result
    form_class = AdminResultCreateForm
    success_url = reverse_lazy('bcdapp:adminresultlist')


class AdminResultDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminresultdelete.html'
    model = Result
    form_class = AdminResultDeleteForm
    success_url = reverse_lazy('bcdapp:adminresultlist')




class AdminSliderListView(AdminMixin, ListView):
    template_name = 'bcdapp/admintemplates/adminsliderlist.html'
    queryset = Slider.objects.filter(deleted_at__isnull=True)
    context_object_name = 'allsliders'


class AdminSliderCreateView(AdminMixin, CreateView):
    template_name = 'bcdapp/admintemplates/adminslidercreate.html'
    form_class = AdminSliderCreateForm
    success_url = reverse_lazy('bcdapp:adminsliderlist')


class AdminSliderUpdateView(AdminMixin, UpdateView):
    template_name = 'bcdapp/admintemplates/adminslidercreate.html'
    model = Slider
    form_class = AdminSliderCreateForm
    success_url = reverse_lazy('bcdapp:adminsliderlist')


class AdminSliderDeleteView(AdminMixin, DeleteMixin):
    template_name = 'bcdapp/admintemplates/adminsliderdelete.html'
    model = Slider
    form_class = AdminSliderDeleteForm
    success_url = reverse_lazy('bcdapp:adminsliderlist')





# client side


class ClientHomeView(TemplateView):
	template_name='bcdapp/clienttemplates/clienthome.html'


class ClientAboutView(TemplateView):
	template_name='bcdapp/clienttemplates/clientabout.html'

class ClientGalleryListView(TemplateView):
	template_name='bcdapp/clienttemplates/clientgallerylist.html'


class ClientBlogListView(TemplateView):
	template_name='bcdapp/clienttemplates/clientbloglist.html'


class ClientContactView(TemplateView):
	template_name='bcdapp/clienttemplates/clientcontact.html'



class ClientAdmissionView(TemplateView):
	template_name='bcdapp/clienttemplates/clientadmission.html'



class ClientFeatureslistView(TemplateView):
	template_name='bcdapp/clienttemplates/Clientfeatureslist.html'



class ClientRoutineListView(TemplateView):
	template_name='bcdapp/clienttemplates/Clientroutinelist.html'



class ClientResultListView(TemplateView):
	template_name='bcdapp/clienttemplates/Clientresultlist.html'