from django.contrib import admin
from .models import *


admin.site.register([Blog, Event, Notice, Staff, Features, 
	Gallery, Organization, Testimonial, Result, Routine, Slider ])
