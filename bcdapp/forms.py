from django import forms
from .models import *



class DateTimeInput(forms.DateInput):
    input_type = 'time'


class AdminLoginForm(forms.Form):
    username = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}))
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}))



class AdminOrganizationCreateForm(forms.ModelForm):
    class Meta:
        model = Organization
        exclude = ['created_at', 'updated_at', 'deleted_at']

    def __init__(self, *args, **kwargs):
        super(AdminOrganizationCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminBlogCreateForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = ['title', 'image', 'content', 'author']

    def __init__(self, *args, **kwargs):
        super(AdminBlogCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminBlogDeleteForm(forms.ModelForm):

    class Meta:
        model = Blog
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminEventCreateForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ['title', 'image', 'date', 'location', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminEventCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminEventDeleteForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminGalleryCreateForm(forms.ModelForm):
    class Meta:
        model = Gallery
        fields = ['title', 'image']

    def __init__(self, *args, **kwargs):
        super(AdminGalleryCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminGalleryDeleteForm(forms.ModelForm):

    class Meta:
        model = Gallery
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminNoticeCreateForm(forms.ModelForm):
    class Meta:
        model = Notice
        fields = ['title', 'date', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminNoticeCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminNoticeDeleteForm(forms.ModelForm):

    class Meta:
        model = Notice
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminStaffCreateForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields = ['name', 'image', 'post', 'phone1', 'email', 'subject']
        
    def __init__(self, *args, **kwargs):
        super(AdminStaffCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminStaffDeleteForm(forms.ModelForm):

    class Meta:
        model = Staff
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminTestimonialCreateForm(forms.ModelForm):
    class Meta:
        model = Testimonial
        fields = ['name', 'post', 'says']

    def __init__(self, *args, **kwargs):
        super(AdminTestimonialCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminTestimonialDeleteForm(forms.ModelForm):

    class Meta:
        model = Testimonial
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }


class AdminFeaturesCreateForm(forms.ModelForm):
    class Meta:
        model = Features
        fields = ['title', 'image', 'content']

    def __init__(self, *args, **kwargs):
        super(AdminFeaturesCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminFeaturesDeleteForm(forms.ModelForm):

    class Meta:
        model = Features
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }





class AdminRoutineCreateForm(forms.ModelForm):
    class Meta:
        model = Routine
        fields = ['grade', 'document']

    def __init__(self, *args, **kwargs):
        super(AdminRoutineCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminRoutineDeleteForm(forms.ModelForm):

    class Meta:
        model = Routine
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }





class AdminResultCreateForm(forms.ModelForm):
    class Meta:
        model = Result
        fields = ['grade', 'document']

    def __init__(self, *args, **kwargs):
        super(AdminResultCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminResultDeleteForm(forms.ModelForm):

    class Meta:
        model = Result
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }



class AdminSliderCreateForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = ['image', 'caption']

    def __init__(self, *args, **kwargs):
        super(AdminSliderCreateForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update(
                {'class': 'form-control'})


class AdminSliderDeleteForm(forms.ModelForm):

    class Meta:
        model = Slider
        fields = ['deleted_at']
        widgets = {
            'deleted_at': forms.HiddenInput(),
        }








