from django.conf.urls import url 
from .views import *


urlpatterns = [

    url(r'^school-admin/logout/$', AdminLogout.as_view(),
        name='adminlogout'),
    url(r'^school-admin/login/$', AdminLogin.as_view(),
        name='adminlogin'),




    url(r'^school-admin/$', AdminHomeView.as_view(), name='adminhome'),
    url(r'^school-admin/school/create/$',
        AdminOrganizationCreateView.as_view(), name='adminorganizationcreate'),
    url(r'^school-admin/school/(?P<pk>\d+)/update/$',
        AdminOrganizationUpdateView.as_view(), name='adminorganizationupdate'),

    url(r'^school-admin/blogs/$',
        AdminBlogListView.as_view(), name='adminbloglist'),
    url(r'^school-admin/blogs/create/$',
        AdminBlogCreateView.as_view(), name='adminblogcreate'),
    url(r'^school-admin/blogs/(?P<pk>\d+)/update/$',
        AdminBlogUpdateView.as_view(), name='adminblogupdate'),
    url(r'^school-admin/blogs/(?P<pk>\d+)/delete/$',
        AdminBlogDeleteView.as_view(), name='adminblogdelete'),

    url(r'^school-admin/events/$',
        AdminEventListView.as_view(), name='admineventlist'),
    url(r'^school-admin/events/create/$',
        AdminEventCreateView.as_view(), name='admineventcreate'),
    url(r'^school-admin/events/(?P<pk>\d+)/update/$',
        AdminEventUpdateView.as_view(), name='admineventupdate'),
    url(r'^school-admin/events/(?P<pk>\d+)/delete/$',
        AdminEventDeleteView.as_view(), name='admineventdelete'),

    url(r'^school-admin/gallery/$',
        AdminGalleryListView.as_view(), name='admingallerylist'),
    url(r'^school-admin/gallery/create/$',
        AdminGalleryCreateView.as_view(), name='admingallerycreate'),
    url(r'^school-admin/gallery/(?P<pk>\d+)/update/$',
        AdminGalleryUpdateView.as_view(), name='admingalleryupdate'),
    url(r'^school-admin/gallery/(?P<pk>\d+)/delete/$',
        AdminGalleryDeleteView.as_view(), name='admingallerydelete'),

    url(r'^school-admin/notices/$',
        AdminNoticeListView.as_view(), name='adminnoticelist'),
    url(r'^school-admin/notices/create/$',
        AdminNoticeCreateView.as_view(), name='adminnoticecreate'),
    url(r'^school-admin/notices/(?P<pk>\d+)/update/$',
        AdminNoticeUpdateView.as_view(), name='adminnoticeupdate'),
    url(r'^school-admin/notices/(?P<pk>\d+)/delete/$',
        AdminNoticeDeleteView.as_view(), name='adminnoticedelete'),

    url(r'^school-admin/staffs/$',
        AdminStaffListView.as_view(), name='adminstafflist'),
    url(r'^school-admin/staffs/create/$',
        AdminStaffCreateView.as_view(), name='adminstaffcreate'),
    url(r'^school-admin/staffs/(?P<pk>\d+)/update/$',
        AdminStaffUpdateView.as_view(), name='adminstaffupdate'),
    url(r'^school-admin/staffs/(?P<pk>\d+)/delete/$',
        AdminStaffDeleteView.as_view(), name='adminstaffdelete'),

    url(r'^school-admin/testimonials/$',
        AdminTestimonialListView.as_view(), name='admintestimoniallist'),
    url(r'^school-admin/testimonials/create/$',
        AdminTestimonialCreateView.as_view(), name='admintestimonialcreate'),
    url(r'^school-admin/testimonials/(?P<pk>\d+)/update/$',
        AdminTestimonialUpdateView.as_view(), name='admintestimonialupdate'),
    url(r'^school-admin/testimonials/(?P<pk>\d+)/delete/$',
        AdminTestimonialDeleteView.as_view(), name='admintestimonialdelete'),



    url(r'^school-admin/features/$',
        AdminFeaturesListView.as_view(), name='adminfeatureslist'),
    url(r'^school-admin/features/create/$',
        AdminFeaturesCreateView.as_view(), name='adminfeaturescreate'),
    url(r'^school-admin/features/(?P<pk>\d+)/update/$',
        AdminFeaturesUpdateView.as_view(), name='adminfeaturesupdate'),
    url(r'^school-admin/features/(?P<pk>\d+)/delete/$',
        AdminFeaturesDeleteView.as_view(), name='adminfeaturesdelete'),






    url(r'^school-admin/routine/$',
        AdminRoutineListView.as_view(), name='adminroutinelist'),
    url(r'^school-admin/routine/create/$',
        AdminRoutineCreateView.as_view(), name='adminroutinecreate'),
    url(r'^school-admin/routine/(?P<pk>\d+)/update/$',
        AdminRoutineUpdateView.as_view(), name='adminroutineupdate'),
    url(r'^school-admin/routine/(?P<pk>\d+)/delete/$',
        AdminRoutineDeleteView.as_view(), name='adminroutinedelete'),




    url(r'^school-admin/result/$',
        AdminResultListView.as_view(), name='adminresultlist'),
    url(r'^school-admin/result/create/$',
        AdminResultCreateView.as_view(), name='adminresultcreate'),
    url(r'^school-admin/result/(?P<pk>\d+)/update/$',
        AdminResultUpdateView.as_view(), name='adminresultupdate'),
    url(r'^school-admin/result/(?P<pk>\d+)/delete/$',
        AdminResultDeleteView.as_view(), name='adminresultdelete'),



    url(r'^school-admin/slider/$',
        AdminSliderListView.as_view(), name='adminsliderlist'),
    url(r'^school-admin/slider/create/$',
        AdminSliderCreateView.as_view(), name='adminslidercreate'),
    url(r'^school-admin/slider/(?P<pk>\d+)/update/$',
        AdminSliderUpdateView.as_view(), name='adminsliderupdate'),
    url(r'^school-admin/slider/(?P<pk>\d+)/delete/$',
        AdminSliderDeleteView.as_view(), name='adminsliderdelete'),    


# client side


	url(r'^$', ClientHomeView.as_view(), name='clienthome'),
	url(r'^about/$', ClientAboutView.as_view(), name='clientabout'),
	url(r'^gallery/$', ClientGalleryListView.as_view(), name='clientgallerylist'),
	url(r'^blog/$', ClientBlogListView.as_view(), name='clientbloglist'),
    url(r'^contact/$', ClientContactView.as_view(), name='clientcontact'),
    url(r'^admission/$', ClientAdmissionView.as_view(), name='clientadmission'),
    url(r'^feestructure/$', ClientFeatureslistView.as_view(), name='Clientfeatureslist'),
    url(r'^routine/$', ClientRoutineListView.as_view(), name='Clientroutinelist'),
    url(r'^result/$', ClientResultListView.as_view(), name='Clientresultlist'),
	


]